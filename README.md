
# Overview

<h4>The python codes is prepared for changing tabular data format into RDF graph model (subject, predicate, object).  The data about E.coli O-antien are organized in table format of csv file and tabular data were changed into RDF triples using the RDFLib, python package. </h4>


## RDFized data for *E. coli* O-antigen structures

### Biochemical Reaction
* GC-RXN.py : RDFication of reactant, product, sugar monoccharide resources for a biochemical reaction.
* GC-Catalysis.py : RDFication of enzyme name and type of catalytic action.
* GC-Protein.py : RDFication of enzyme reference.
* GC-Saccharide_2020_02.py : RDFication of saccharides in reactant and product.
   
### Pathway
* GC-BiochemicalPathwayStep.py : RDFization of order of biochemical reaction.
* GC-Pathway.py : RDFization of reactions and enzymes composing antigen structure.

## Requirement
* <h5>Preparation of table data</h5>

    The subject resources is placed in the leftmost column in the table.

    The property resources have to be placed in heading row in the table.

    The object resources is placed in the cell of the relevant property column.
* <h5>Python 3.7.0</h5>
 
    (https://docs.python.org/3.7/using/index.html)
* <h5>RDFLib 4.2.2</h5>

    (https://rdflib.readthedocs.io/en/4.2.2/)

## License
    
    Creative Commons Attribution 4.0 International License.


---
