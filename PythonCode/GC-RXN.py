
import csv
import rdflib
from rdflib import Graph
from rdflib.namespace import RDF
from rdflib import URIRef

# ontology
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSV
f = open('GC-Reaction_2019_05.csv','rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

for row in dataReader:
    # Class row of Instances
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]

    if row[0] != "BiochemicalReaction":

        gc_rxn= URIRef(rxn+ col_A)
        sm_left = URIRef(rxn + col_B)
        sm_right = URIRef(rxn + col_C)
        sm_stoichiometry = URIRef(rxn + col_D)
        sm_type = URIRef(bp + col_E)
        if col_B != "":
            g.add((gc_rxn, bp.left, sm_left))
        if col_C != "":
            g.add((gc_rxn, bp.right, sm_right))
        if col_E != "":
            g.add((gc_rxn, RDF.type, sm_type))
        
        g.add((gc_rxn, bp.participantStoichiometry, sm_stoichiometry))

g.serialize(destination="GC_RXN_RDF_2019.ttl", format='turtle')
